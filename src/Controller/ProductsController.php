<?php

namespace App\Controller;

use App\Entity\Products;
use App\Repository\ProductsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends AbstractController
{
    /**
     * @Route("/product", name="app_product")
     */
    public function index(): Response
    {
        return $this->json([
            'name' => 'Product 1',
            'description' => 'description product 1',
            'prix' => '120'
        ]);
    }

    /**
     * @Route("/product/create", name="create_product")
     */
    public function create(\Doctrine\Persistence\ManagerRegistry $doctrine, Request $request)
    {
        $content = $request->getContent();
        $param = json_decode($content, true);
        $product = new Products();
        $product->setName($param['name']);
        $product->setPrix($param['prix']);
        $product->setDescription($param['description']);
        $entityManager = $doctrine->getManager();
        $entityManager->persist($product);
        $entityManager->flush();
        $message = "Product was successfully added";
        return new JsonResponse(array("message: $message"));
    }

    /**
     * @Route("/product/delete/{id}", name="delete_product")
     */
    public function delete(\Doctrine\Persistence\ManagerRegistry $doctrine, Request $request, int $id, ProductsRepository $repository): Response
    {
        $entityManager = $doctrine->getManager();
        $product = $repository->find($id);
        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        $entityManager->remove($product);
        $entityManager->flush();
        $message = "Product was successfully deleted";
        return new JsonResponse(array("message: $message"));
    }

    /**
     * @Route("/product/update/{id}", name="update_product")
     */
    public function update(\Doctrine\Persistence\ManagerRegistry $doctrine, Request $request, int $id, ProductsRepository $repository): JsonResponse{
        $entityManager = $doctrine->getManager();
        $product = $repository->find($id);
        $data = json_decode($request->getContent(), true);
        $product->setName($data["name"]);
        $product->setDescription($data["description"]);
        $product->setPrix($data["price"]);
        $entityManager->persist($product);
        $entityManager->flush();
        $message = "Product was successfully added";
        return new JsonResponse(array("message: $message"));
    }
}
